import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Event extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Event",
      extends: null,
      instantiable: true,
      timestamps: true,
      permissions: {
        Secure: ["admin"],
      },
      fields: {
        id: {
          type: ScalarTypes.ID,
          asName: "e_id",
          key: true,
          required: true,
          input: false,
        },
        name: {
          type: ScalarTypes.String,
          asName: "e_name",
        },
        ownerId: {
          type: ScalarTypes.Integer,
          asName: "c_id",
        },
        postedDate: {
          type: ScalarTypes.DateTime,
          asName: "posted_date",
        },
        startDate: {
          type: ScalarTypes.DateTime,
          asName: "e_start_date",
        },
        lastChangedDate: {
          type: ScalarTypes.DateTime,
          asName: "e_last_changed",
        },
        description: {
          type: ScalarTypes.String,
          asName: "e_description",
        },
        sellTickets: {
          type: ScalarTypes.Boolean,
          asName: "e_sell_tickets",
        },
        siteEventFee: {
          type: ScalarTypes.Float,
          asName: "site_event_fee",
        },
        sitename: {
          type: ScalarTypes.String,
        },
        venue: {
          type: ScalarTypes.String,
          asName: "e_venue",
        },
        city: {
          type: ScalarTypes.String,
          asName: "e_city",
        },
        state: {
          type: ScalarTypes.String,
          asName: "e_state",
        },
        country: {
          type: ScalarTypes.String,
          asName: "e_country",
        },
        zip: {
          type: ScalarTypes.String,
          asName: "e_zip",
        },
        zipcode: {
          type: ScalarTypes.ObjectRef,
          objectType: "Zipcode",
          refField: ["zip", "country"],
          toRefField: ["zipcode", "country"],
          refFieldType: ScalarTypes.String,
          array: false,
          input: false,
        },
        approved: {
          type: ScalarTypes.Boolean,
          asName: "e_approved",
        },
        approvedDate: {
          type: ScalarTypes.DateTime,
          asName: "approved_date",
        },
        approvedById: {
          type: ScalarTypes.Integer,
          asName: "approval_c_id",
        },
        heldById: {
          type: ScalarTypes.Integer,
          asName: "flagged_c_id",
        },
        heldDate: {
          type: ScalarTypes.DateTime,
          asName: "flagged_date",
        },
        heldReason: {
          type: ScalarTypes.String,
          asName: "flagged_reason",
        },
        onHold: {
          type: ScalarTypes.Boolean,
          symbolic: true,
          input: false,
        },
        complete: {
          type: ScalarTypes.Boolean,
          asName: "e_complete",
        },
        activated: {
          type: ScalarTypes.Boolean,
          asName: "e_activated",
        },
        defaultUserEvent: {
          type: ScalarTypes.Boolean,
          asName: "e_default_user_event",
        },
        claimed: {
          type: ScalarTypes.Boolean,
          symbolic: true,
          input: false,
        },
        claimedDate: {
          type: ScalarTypes.DateTime,
          symbolic: true,
          input: false,
        },
        claimedBy: {
          type: ScalarTypes.String,
          symbolic: true,
          input: false,
        },
        testSets: {
          type: ScalarTypes.ObjectRef,
          objectType: "EventTestSet",
          refFieldType: ScalarTypes.Integer,
          toRefField: "eventId",
          sort: { field: "timestamp", dir: "DESC" },
          params: "query",
          input: false,
          array: true,
        },
        queueClaims: {
          type: ScalarTypes.ObjectRef,
          objectType: "EventQueueClaim",
          refFieldType: ScalarTypes.Integer,
          toRefField: "eventId",
          sort: { field: "claimTimestamp", dir: "DESC" },
          params: "query",
          input: false,
          array: true,
        },
        owner: {
          type: ScalarTypes.ObjectRef,
          objectType: "Client",
          refField: "ownerId",
          refFieldType: ScalarTypes.Integer,
          input: false,
        },
        approvedBy: {
          type: ScalarTypes.ObjectRef,
          objectType: "Client",
          refField: "approvedById",
          refFieldType: ScalarTypes.Integer,
          input: false,
        },
        heldBy: {
          type: ScalarTypes.ObjectRef,
          objectType: "Client",
          refField: "heldById",
          refFieldType: ScalarTypes.Integer,
          input: false,
        },
        cluster: {
          type: ScalarTypes.ObjectRef,
          objectType: "EventCluster",
          refField: "id",
          refFieldType: ScalarTypes.Integer,
          relationshipName: "clusterMembers",
          input: false,
        },
      },
    }
  }
}
