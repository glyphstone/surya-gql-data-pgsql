import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Category extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  typeSpec() {
    return {
      name: "Category",
      extends: null,
      instantiable: true,
      permissions: {
        secure: ["ADMINISTRATOR"],
      },
      fields: {
        id: {
          type: ScalarTypes.Integer,
          asName: "category_id",
          key: true,
          required: true,
          input: false,
        },
        name: {
          type: ScalarTypes.String,
          asName: "category_name",
        },
        parentId: {
          type: ScalarTypes.Integer,
          asName: "parent_id",
        },
        ordering: {
          type: ScalarTypes.Integer,
        },
        color: {
          type: ScalarTypes.String,
        },
        mainPage: {
          type: ScalarTypes.Boolean,
          asName: "mainpage",
        },
        pathName: {
          type: ScalarTypes.String,
          symbolic: true,
          input: false,
        },
        parent: {
          type: ScalarTypes.ObjectRef,
          objectType: "Category",
          refField: "parentId",
          refFieldType: ScalarTypes.Integer,
          input: false,
        },
        children: {
          type: ScalarTypes.ObjectRef,
          objectType: "Category",
          refField: "id",
          refFieldType: ScalarTypes.Integer,
          toRefField: "parent_id",
          sort: { field: "claimTimestamp", dir: "DESC" },
          params: "query",
          input: false,
          array: true,
        },
      },
    }
  }
}
