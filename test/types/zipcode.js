import { BaseType } from "@brownpapertickets/surya-gql-types"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"

export class Zipcode extends BaseType {
  constructor(data = {}) {
    super(data)
  }

  childTypes() {
    return {}
  }

  typeSpec() {
    return {
      name: "Zipcode",
      extends: null,
      instantiable: true,
      permissions: {
        secure: ["ADMINISTRATOR"],
      },
      fields: {
        id: {
          type: ScalarTypes.Integer,
          asName: "zipcode_id",
        },
        zipcode: {
          type: ScalarTypes.String,
        },
        city: {
          type: ScalarTypes.String,
        },
        state: {
          type: ScalarTypes.String,
        },
        county: {
          type: ScalarTypes.String,
        },
        country: {
          type: ScalarTypes.String,
        },
        areaCode: {
          type: ScalarTypes.String,
          asName: "areacode",
        },
        cityType: {
          type: ScalarTypes.String,
          asName: "cityType",
        },
        cityAliasAbbreviation: {
          type: ScalarTypes.String,
          asName: "cityaliasabbreviation",
        },
        cityAliasName: {
          type: ScalarTypes.String,
          asName: "cityaliasname",
        },
        latitude: {
          type: ScalarTypes.String,
        },
        longitude: {
          type: ScalarTypes.String,
        },
        timezone: {
          type: ScalarTypes.Integer,
        },
        elevation: {
          type: ScalarTypes.Integer,
          asName: "elevation",
        },
        countyFips: {
          type: ScalarTypes.String,
          asName: "countyfips",
        },
        daylightSaving: {
          type: ScalarTypes.String,
          asName: "daylightsaving",
        },
        preferredLastLineKey: {
          type: ScalarTypes.String,
          asName: "preferredlastlinekey",
        },
        classificationCode: {
          type: ScalarTypes.String,
          asName: "classificationcode",
        },
        multiCounty: {
          type: ScalarTypes.String,
          asName: "multicounty",
        },
        stateFIPS: {
          type: ScalarTypes.String,
          asName: "statefips",
        },
        cityStateKey: {
          type: ScalarTypes.String,
          asName: "citystatekey",
        },
        cityAliasCode: {
          type: ScalarTypes.String,
          asName: "cityaliascode",
        },
        primaryRecord: {
          type: ScalarTypes.String,
          asName: "primaryrecord",
        },
        cityStateKey: {
          type: ScalarTypes.String,
          asName: "citystatekey",
        },
        cityMixedCase: {
          type: ScalarTypes.String,
          asName: "citymixedcase",
        },
        cityAliasMixedCase: {
          type: ScalarTypes.String,
          asName: "cityaliasmixedcase",
        },
      },
    }
  }
}
