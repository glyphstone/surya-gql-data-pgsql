import { PGSqlQuery } from "../src/pgSqlQuery"
import { MockLog } from "./mockLog"
import { Event } from "./types/event"
import { EventQueueClaim } from "./types/eventQueueClaim"
import { Client } from "./types/client"
import { EventTestSet } from "./types/eventTestSet"
import { EventCluster } from "./types/eventCluster"
import { Category } from "./types/category"
import { Zipcode } from "./types/zipcode"

let container
let queryEngine

beforeAll(() => {
  container = { log: new MockLog() }

  let typeMap = {
    Event: new Event().typeSpec(),
    EventQueueClaim: new EventQueueClaim().typeSpec(),
    Client: new Client().typeSpec(),
    EventTestSet: new EventTestSet().typeSpec(),
    EventCluster: new EventCluster().typeSpec(),
    Category: new Category().typeSpec(),
    Zipcode: new Zipcode().typeSpec(),
  }
  queryEngine = new PGSqlQuery(container, new Event(), typeMap)
})

describe("pgSqlQuery Event Parser Tests", () => {
  describe("basic predicate op coverage", () => {
    test("test Event pgSql filter = production", async () => {
      const filter = queryEngine.queryFilter("name = 'BrownPaperTickets'")
      //console.log(filter)
      expect(filter.where).toEqual("t0.e_name = $1")
      expect(filter.params[0]).toEqual("BrownPaperTickets")
    })

    test("test filter != production", async () => {
      const filter = queryEngine.queryFilter("name != 'BrownPaperTickets'")
      //console.log(mongoFilter)
      expect(filter.where).toEqual("t0.e_name != $1")
      expect(filter.params[0]).toEqual("BrownPaperTickets")
    })

    test("test filter > production", async () => {
      const filter = queryEngine.queryFilter("name > 'BrownPaperTickets'")
      expect(filter.where).toEqual("t0.e_name > $1")
      expect(filter.params[0]).toEqual("BrownPaperTickets")
    })

    test("test  filter >= production", async () => {
      const filter = queryEngine.queryFilter("name >= 'BrownPaperTickets'")
      expect(filter.where).toEqual("t0.e_name >= $1")
      expect(filter.params[0]).toEqual("BrownPaperTickets")
    })

    test("test  filter < production", async () => {
      const filter = queryEngine.queryFilter("name < 'BrownPaperTickets'")
      expect(filter.where).toEqual("t0.e_name < $1")
      expect(filter.params[0]).toEqual("BrownPaperTickets")
    })

    test("test  filter <= production", async () => {
      const filter = queryEngine.queryFilter("name <= 'BrownPaperTickets'")
      expect(filter.where).toEqual("t0.e_name <= $1")
      expect(filter.params[0]).toEqual("BrownPaperTickets")
    })

    test("test  filter matches production", async () => {
      const filter = queryEngine.queryFilter("name MATCHES 'Brown'")
      expect(filter.where).toEqual("t0.e_name ~* $1")
      expect(filter.params[0]).toEqual("Brown")
    })

    test("test filter in production", async () => {
      const filter = queryEngine.queryFilter(
        "name in ( 'Brown', 'Paper', 'Tickets' )"
      )
      expect(filter.where).toEqual("t0.e_name IN ( $1, $2, $3 ) ")
      expect(filter.params).toEqual(["Brown", "Paper", "Tickets"])
    })

    test("test  filter !IN production", async () => {
      const filter = queryEngine.queryFilter(
        "name !IN ( 'Brown', 'Paper', 'Tickets' )"
      )
      expect(filter.where).toEqual("t0.e_name NOT IN ( $1, $2, $3 ) ")
      expect(filter.params).toEqual(["Brown", "Paper", "Tickets"])
    })

    test("test  filter exists production", async () => {
      const filter = queryEngine.queryFilter("name EXISTS")
      expect(filter.where).toEqual("t0.e_name IS NOT NULL ")
    })

    test("test filter !EXISTS production", async () => {
      const filter = queryEngine.queryFilter("name !EXISTS")
      expect(filter.where).toEqual("t0.e_name IS NULL ")
    })
  })

  describe("Complex and fail scenarios ", () => {
    test("test  filter complex and/or", async () => {
      const filter = queryEngine.queryFilter(
        "name matches 'Brown' AND description = 'Seattle' OR name matches 'tickets' AND description  = 'London' "
      )
      //console.log(JSON.stringify(filter))

      expect(filter.where).toEqual(
        "( ( t0.e_name ~* $1 AND t0.e_description = $2 ) OR ( t0.e_name ~* $3 AND t0.e_description = $4 ) )"
      )
      expect(filter.params).toEqual(["Brown", "Seattle", "tickets", "London"])
    })

    test("test filter fail on bad field name", async () => {
      try {
        const filter = queryEngine.queryFilter("doesNotExists = 'isBogus' ")
        expect(true).toBe(false)
      } catch (ex) {
        const msg = ex.message
        console.log(`Take this branch: ${msg}`)
        expect(msg).toEqual(
          "Query Parser error: Field: 'doesNotExists' does not exist"
        )
      }
    })

    test("test no filter but sort clause", async () => {
      const filter = queryEngine.queryFilter("  ", "id DESC")
      console.log(JSON.stringify(filter))
      expect(filter.where).toEqual(null)
      expect(filter.sortSpec).toEqual("t0.e_id DESC")
    })
  })

  describe("Compound join queries", () => {
    const MQuery =
      "complete = true AND activated = true AND defaultUserEvent = false AND approved = false AND queueClaims.client.clientName IN (  'brianm' ) AND queueClaims.unclaimTimestamp !EXISTS"
    const MXQuery =
      "complete = true AND activated = true AND defaultUserEvent = false AND approved = false AND testSets.0.result in ( 'pass', 'warn' ) "
    const MXSort = "testSets.0.result"

    const OneMultiColQuery = "state = 'IL' and zipcode.city = 'CHICAGO'"
    const OneMultiColSort = "zipcode.latitude DESC"

    test("multiple and double nested", async () => {
      const filter = queryEngine.queryFilter(MQuery)
      //  console.log(JSON.stringify(filter))

      expect(filter.where).toEqual(
        "( t0.e_complete = $1 AND ( t0.e_activated = $2 AND ( t0.e_default_user_event = $3 AND ( t0.e_approved = $4 AND ( t2.client_name IN ( $5 )  AND t1.unclaim_timestamp IS NULL  ) ) ) ) )"
      )
      expect(filter.params).toEqual([true, true, false, false, "brianm"])

      //console.log(`>>> filter: ${JSON.stringify(filter, null, 2)}`)
      const joinQueueClaims = filter.joinSpecs["t0..queueClaims"]
      const joinClient = filter.joinSpecs["t1..client"]
      expect(joinQueueClaims).toEqual({
        groupBy: "t0.e_id",
        idColumn: "t1.claim_id",
        joinFromColumn: "t0.e_id",
        joinMode: "M",
        onClause: "t0.e_id = t1.event_claimed_id ",
        tableAlias: "t1",
        typeName: "EventQueueClaim",
      })

      expect(joinClient).toEqual({
        joinMode: "1",
        idColumn: ["t2.c_id"],
        joinFromColumn: ["t1.claim_client_id"],
        onClause: "t1.claim_client_id = t2.c_id ",
        tableAlias: "t2",
        typeName: "Client",
      })
    })

    test("Multi-indexed filter and sort", async () => {
      const filter = queryEngine.queryFilter(MXQuery, MXSort)
      // console.log(`MX query: ${JSON.stringify(filter, null, 2)}`)

      expect(filter.where).toEqual(
        "( t0.e_complete = $1 AND ( t0.e_activated = $2 AND ( t0.e_default_user_event = $3 AND ( t0.e_approved = $4 AND t1.etsetr_result IN ( $5, $6 )  ) ) ) )"
      )
      expect(filter.params).toEqual([true, true, false, false, "pass", "warn"])

      const joinTestSets = filter.joinSpecs["t0.0.testSets"]

      expect(joinTestSets).toEqual({
        tableAlias: "t1",
        typeName: "EventTestSet",
        joinMode: "MX",
        onClause:
          "t1.etsetr_id = ( SELECT t1j.etsetr_id  FROM <<JOIN-TABLE>> t1j WHERE t0.e_id = t1j.etsetr_event_id ORDER BY t1j.etsetr_timestamp DESC offset 0 limit 1 )",
        groupBy: "t0.e_id",
        idColumn: "t1.etsetr_id",
        joinFromColumn: "t0.e_id",
      })

      expect(filter.sortSpec).toEqual("t1.etsetr_result ASC")
    })

    test("1 query with multi-col join field", async () => {
      const filter = queryEngine.queryFilter(OneMultiColQuery, OneMultiColSort)
      // console.log(`OneMultiCol: ${JSON.stringify(filter, null, 2)}`)

      expect(filter.where).toEqual("( t0.e_state = $1 AND t1.city = $2 )")
      expect(filter.params).toEqual(["IL", "CHICAGO"])

      const joinZipcode = filter.joinSpecs["t0..zipcode"]

      expect(joinZipcode.tableAlias).toEqual("t1")
      expect(joinZipcode.typeName).toEqual("Zipcode")
      expect(joinZipcode.joinMode).toEqual("1")
      expect(joinZipcode.onClause).toEqual(
        "t0.e_zip = t1.zipcode  AND t0.e_country = t1.country "
      )
      expect(filter.sortSpec).toEqual("t1.latitude DESC")
      expect(filter.symbolics).toEqual([])
    })
  })

  describe("X Intermediate join queries", () => {
    const XQuery = "cluster.allowTop10 = true "
    const XSort = "testSets.0.result"

    test("multiple and double nested", async () => {
      const filter = queryEngine.queryFilter(XQuery)

      expect(filter.where).toEqual("t1.c_allowtop10 = $1")
      expect(filter.params).toEqual([true])

      //console.log(`>>> filter: ${JSON.stringify(filter, null, 2)}`)
      const joinCluster = filter.joinSpecs["t0..cluster"]

      expect(joinCluster).toEqual({
        joinMode: "X",
        tableAlias: "t1",
        xTableAlias: "xt1",
        relationshipName: "clusterMembers",
        typeName: "EventCluster",
        onClause: "t1.cluster_id = xt1.<<X_KEY_COLUMN>>",
        xOnClause: "t0.e_id = xt1.<<X_JOIN_COLUMN>>",
        joinFromColumn: "t0.e_id",
        idColumn: "t1.cluster_id",
      })
    })

    test("X Intermediate two-level filter and sort", async () => {
      const filter = "cluster.categories.id in ( 200114, 200060, 200092 )  "
      const sort = "cluster.categories.ordering"
      const filterSpec = queryEngine.queryFilter(filter, sort)
      //console.log(`X^2 query: ${JSON.stringify(filterSpec, null, 2)}`)

      expect(filterSpec.where).toEqual("t2.category_id IN ( $1, $2, $3 ) ")
      expect(filterSpec.params).toEqual([200114, 200060, 200092])

      const joinCluster = filterSpec.joinSpecs["t0..cluster"]
      const joinCategories = filterSpec.joinSpecs["t1..categories"]

      expect(joinCluster).toEqual({
        joinMode: "X",
        tableAlias: "t1",
        xTableAlias: "xt1",
        relationshipName: "clusterMembers",
        typeName: "EventCluster",
        onClause: "t1.cluster_id = xt1.<<X_KEY_COLUMN>>",
        xOnClause: "t0.e_id = xt1.<<X_JOIN_COLUMN>>",
        joinFromColumn: "t0.e_id",
        idColumn: "t1.cluster_id",
      })

      expect(joinCategories).toEqual({
        joinMode: "X",
        tableAlias: "t2",
        xTableAlias: "xt2",
        relationshipName: "eventClusterCategories",
        typeName: "Category",
        onClause: "t2.category_id = xt2.<<X_KEY_COLUMN>>",
        xOnClause: "t1.cluster_id = xt2.<<X_JOIN_COLUMN>>",
        joinFromColumn: "t1.cluster_id",
        idColumn: "t2.category_id",
      })

      expect(filterSpec.sortSpec).toEqual("t2.ordering ASC")
    })
  })
})
