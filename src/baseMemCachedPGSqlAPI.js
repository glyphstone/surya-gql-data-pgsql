import { BasePGSqlAPI } from "./basePGSqlAPI"
import { MemObjectCache } from "@brownpapertickets/surya-gql-data"

export class BaseMemCachedPGSqlAPI extends BasePGSqlAPI {
  constructor(container, type, typeMap, config, ttl = 300, checkPeriod = 150) {
    super(container, type, typeMap, config)
    this.cache = new MemObjectCache(container, type, ttl, checkPeriod)
  }

  // --------------------------------------------------

  async query(offset, limit, filter, sort, session) {
    let result = await this.cache.getQueryResult(offset, limit, filter, sort)
    if (!result) {
      result = await super.query(offset, limit, filter, sort, session)
      this.cache.setQueryResult(offset, limit, filter, sort, result)
    }
    return result
  }

  async get({ id, session }) {
    let obj = await this.cache.getObject(id)
    if (!obj) {
      obj = await super.get(id, session)
      const setres = await this.cache.setObject(id, obj)
    }
    return obj
  }

  async create(obj, session) {
    let result = await super.create(obj, session)
    return result
  }

  async update(id, obj, session) {
    let result = await super.update(id, obj, session)
    this.cache.purgeObject(id)
    return result
  }

  async delete(id, session) {
    let result = await super.delete(id, obj, session)
    this.cache.purgeObject(id)
    return result
  }
}
