import { Client as PGClient } from "pg"
import { BaseDataInterface } from "@brownpapertickets/surya-gql-data"

export class PGSql extends BaseDataInterface {
  constructor(container) {
    super(container)
    this.pgClient = null
  }

  async connect(connectionString, isProduction) {
    this.log.info(`GraphQL PGSql Connect: ${connectionString}`)
    this.pgClient = new PGClient({
      connectionString: connectionString,
    })
    this.pgClient.connect()
    return this
  }

  async disconnect() {
    if (this.pgClient) {
      this.pgClient.end()
    }
  }
}
