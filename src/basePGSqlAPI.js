import { PGSqlQuery } from "./pgSqlQuery"
import { ScalarTypes } from "@brownpapertickets/surya-gql-scalar"
import { BaseCRUDAPI } from "@brownpapertickets/surya-gql-data"

const DEFAULT_LIMIT = 100

export class BasePGSqlAPI extends BaseCRUDAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
    this.pgsqlQuery = new PGSqlQuery(container, type, typeMap, config)
    this.symbolics = {}
    this.joinSpecs = this.config.joinSpecs || {}
  }

  /**
   * Set table name in manifest as
    data: {
    interface: "PGSql",
    api: "MyAPI",
    config: { table: "my-table" },
    },
   * else override this method directly to return the table name.
   * Else.. table name will default to the type name as set in the TypeSpec.
   */
  getTableName() {
    return this.config.table || this.getTypeName().toLowerCase()
  }

  /* This is needed for intermediate (M/M joins)
  see joinByIntermediate()
  */
  getKeyColumn() {
    return this.config.keyColumn || null
  }

  getDefaultSort() {
    return this.config.defaultSort || this.config.keyColumn
  }

  getInsertDefaults() {
    return this.config.insertDefaults || null
  }

  getJoinSpec(joinName) {
    return this.joinSpecs[joinName]
  }

  setSymbolic(name, sql) {
    this.symbolics[name] = sql
  }

  getSymbolic(name) {
    return this.symbolics[name]
  }

  // --------------------------------------------------

  async query(
    offset = 0,
    limit = DEFAULT_LIMIT,
    filter = "",
    sort = "",
    session = null
  ) {
    const tableName = this.getTableName()
    const getTotal = "getTotal" in this.config ? !!this.config.getTotal : true
    const tableAlias = "t0"
    const filterSpec = this.pgsqlQuery.queryFilter(filter, sort)

    this.expandSymbolics(filterSpec)

    const whereClause =
      filterSpec.where && filterSpec.where.trim().length > 0
        ? `WHERE ${filterSpec.where}`
        : ""

    const joinResponse = this.genJoinClause(filterSpec.joinSpecs)

    const sortSpec = filterSpec.sortSpec
    const orderByClause =
      sortSpec && sortSpec.trim().length > 0 ? `ORDER BY ${sortSpec}` : ""

    let sqlSelect = this.genSelect([], tableAlias)
    let total = null

    if (getTotal) {
      sqlSelect += ", count(*) OVER() AS _total"
    }

    const query = `SELECT ${sqlSelect} FROM ${tableName} ${tableAlias} ${joinResponse.joinClause} ${whereClause} ${joinResponse.groupByClause} ${orderByClause} LIMIT ${limit} OFFSET ${offset}`

    this.log.info(`Query: ${query}`)
    const queryResult = await this.db.pgClient.query(query, filterSpec.params)
    const rows = queryResult.rows
    if (getTotal) {
      if (rows.length > 0) {
        total = rows[0]["_total"]
      }
    }

    let items = rows.map((row) => {
      return Object.create(this.type).loadData(row)
    })
    const result = {
      items: items,
      total: total,
      count: rows.length,
      offset: offset,
    }

    return result
  }

  genJoinClause(joinSpecs) {
    let joinClause = ""
    let groupByClause = ""

    if (joinSpecs) {
      Object.keys(joinSpecs).forEach((key) => {
        const spec = joinSpecs[key]
        const typeName = spec.typeName
        const toAPI = this.db.getAPI(typeName)
        const tableName = toAPI.getTableName()
        let onClause = spec.onClause

        switch (spec.joinMode) {
          case "M":
            groupByClause = ` GROUP BY ${spec.groupBy} `
            break
          case "MX":
            // indexed multi-join
            onClause = onClause.replace("<<JOIN-TABLE>>", tableName) // no group by needed...
            break
          case "X":
            // M-M join through intermediate table
            const xJoinSpec = toAPI.getJoinSpec(spec.relationshipName)
            let xOnClause = spec.xOnClause
            onClause = onClause.replace("<<X_KEY_COLUMN>>", xJoinSpec.keyColumn)
            xOnClause = xOnClause.replace(
              "<<X_JOIN_COLUMN>>",
              xJoinSpec.joinColumn
            )
            joinClause += `
            LEFT OUTER JOIN ${xJoinSpec.table} ${spec.xTableAlias} ON ${xOnClause}`
            break
          case "1":
            break
          default:
            throw new Error(`Join mode ${spec.joinMode} not supported`)
        }

        joinClause += ` LEFT OUTER JOIN ${tableName} ${spec.tableAlias} ON ${onClause}`
      })
    }

    return {
      joinClause,
      groupByClause,
    }
  }

  // if symbolics present in where clause, if possible, replace with SQL inserts from API
  expandSymbolics(filterSpec) {
    if (filterSpec.symbolics && filterSpec.symbolics.length > 0) {
      let where = filterSpec.where
      let sortSpec = filterSpec.sortSpec
      filterSpec.symbolics.forEach((symbolic) => {
        const symParts = symbolic.split(".")
        let symbolicText = this.getAPISymbolic(
          symParts[0],
          symParts[1],
          filterSpec
        )
        if (symbolicText) {
          symbolicText = `(${symbolicText})`
          where = where.replace(symbolic, symbolicText)
          sortSpec = sortSpec.replace(symbolic, symbolicText)
        } else {
          throw new Error(`Query by symbolic field: ${symbolic} not supported`)
        }
      })
      filterSpec.where = where
      filterSpec.sortSpec = sortSpec
    }
  }

  getAPISymbolic(tableAlias, symbolicName, filterSpec) {
    let symAPI = this
    const keyColumn = this.getKeyColumnName()
    let symIdColumn = `t0.${keyColumn}`

    if (tableAlias != "t0") {
      const { api, idColumn } = this.getAPIByAlias(
        tableAlias,
        filterSpec.joinSpecs
      )

      symAPI = api
      symIdColumn = idColumn
    }
    if (!symAPI) {
      throw new Error(`API not found for ${symbolicName}`)
    }
    let symbolic = symAPI.getSymbolic(symbolicName)
    if (symbolic) {
      // replace join field value
      symbolic = symbolic.replace("$1", symIdColumn)
      // if parameters, replace parameters by position
      const qualifiedSymbolicName = `${tableAlias}.${symbolicName}`
      const symbolicParams =
        filterSpec.symbolicParams[qualifiedSymbolicName] || []
      for (let i = 0; i < symbolicParams.length; i++) {
        const paramVal = symbolicParams[i]
        const token = `{${i + 1}}`
        symbolic = symbolic.replace(token, paramVal)
      }
    } else {
      throw new Error(
        `Cannot filter by symbolic field '${symbolicName}'. Not implemented in the API`
      )
    }

    return symbolic
  }

  getAPIByAlias(alias, joinSpecs) {
    let api = null
    let spec = null
    let idColumn = null
    const keys = Object.keys(joinSpecs)
    keys.forEach((k) => {
      if (joinSpecs[k].tableAlias == alias) {
        spec = joinSpecs[k]
      }
    })

    if (spec) {
      api = this.db.getAPI(spec.typeName)
      const type = api.getType()
      const keyFieldName = type.getKeyField()
      //TODO: factor this with getKeyColumnName()
      if (keyFieldName) {
        const fields = type.typeSpec().fields
        const keyField = fields[keyFieldName]
        idColumn = keyField.asName ? keyField.asName : keyFieldName
      }
    }

    return {
      api,
      idColumn,
    }
  }

  async get(id, session = null) {
    const tableName = this.getTableName()
    const tableAlias = "t1"

    const keyColumnName = this.getKeyColumnName()
    if (!keyColumnName) {
      throw new Error(
        `no key field specified for type ${this.type.typeSpec().name}`
      )
    }
    const sqlSelect = this.genSelect([], tableAlias)
    const whereClause = `WHERE ${tableAlias}.${keyColumnName} = $1`
    const query = `SELECT ${sqlSelect} FROM ${tableName} ${tableAlias} ${whereClause}`

    const queryResult = await this.db.pgClient.query(query, [id])
    const rowCount = queryResult.rowCount
    let obj
    if (rowCount == 1) {
      const row = queryResult.rows[0]
      obj = Object.create(this.type).loadData(row)
    } else {
      if (rowCount == 0) {
        throw new Error("Object not found")
      } else {
        throw new Error("Key not unique!")
      }
    }

    return obj
  }

  async create(obj, session) {
    const tableName = this.getTableName()
    const keyColumnName = this.getKeyColumnName()
    const insertDefaults = this.getInsertDefaults()
    const insertSpec = this.parseForInsert(obj, insertDefaults)

    const insertStatement = `INSERT INTO ${tableName} ( ${insertSpec.columns}) VALUES( ${insertSpec.paramTokens} ) RETURNING ${keyColumnName}`

    this.log.info(
      `PGSQL insert statement: ${insertStatement} params: ${JSON.stringify(
        insertSpec.params
      )}`
    )
    let objOut = obj
    let queryResult = null
    try {
      queryResult = await this.db.pgClient.query(
        insertStatement,
        insertSpec.params
      )
    } catch (ex) {
      this.log.error(`Error inserting row: ${ex.message}`)
      throw ex
    }

    const rowCount = queryResult.rowCount
    if (rowCount == 1) {
      const row = queryResult.rows[0]
      const newId = row[keyColumnName]
      objOut = await this.get(newId)
    } else {
      if (rowCount == 0) {
        throw new Error("Object not created")
      } else {
        throw new Error("Key not unique!")
      }
    }

    return objOut
  }

  parseForInsert(obj, insertDefaults) {
    let paramN = 1
    let params = []
    let columnList = []
    let paramTokens = ""
    if (insertDefaults) {
      Object.keys(insertDefaults).forEach((k) => {
        const colName = this.pgsqlQuery.getColumnName(k, null, null, false)
        const defVal = insertDefaults[k]
        columnList.push(colName)
        paramTokens += `${defVal}, `
      })
    }

    paramTokens += Object.keys(obj)
      .map((l) => `$${paramN++}`)
      .join(", ")
    Object.keys(obj).forEach((fieldName) => {
      if (!columnList.includes(fieldName)) {
        let val = obj[fieldName]
        const { fieldSpec } = this.pgsqlQuery.getFieldSpec(fieldName)

        if (fieldSpec.type == ScalarTypes.DateTime) {
          val = new Date(val).toLocaleString()
        }

        params.push(val)
        const colName = this.pgsqlQuery.getColumnName(
          fieldName,
          null,
          null,
          false
        )
        columnList.push(colName)
      } else {
        this.log.warn(
          `field ${fieldName} is overridden by default value in manifest`
        )
      }
    })

    const columns = columnList.join(", ")

    return {
      columns,
      paramTokens,
      params,
    }
  }

  async update(id, obj, session) {
    const tableName = this.getTableName()
    const keyColumnName = this.getKeyColumnName()

    const updateSpec = this.parseForUpdate(id, obj)
    const updateStatement = `UPDATE ${tableName} SET ${updateSpec.sets} WHERE ${keyColumnName} = $1`

    this.log.info(
      `Update statement: ${updateStatement} params: ${JSON.stringify(
        updateSpec.params
      )}`
    )

    const queryResult = await this.db.pgClient.query(
      updateStatement,
      updateSpec.params
    )

    const objOut = await this.get(id)
    return objOut
  }

  parseForUpdate(id, obj) {
    let params = [id]
    let paramN = 2
    let setList = []

    Object.keys(obj).forEach((fieldName) => {
      let val = obj[fieldName]
      const { fieldSpec } = this.pgsqlQuery.getFieldSpec(fieldName)

      if (fieldSpec.type == ScalarTypes.DateTime) {
        if (val != null) {
          val = new Date(val).toLocaleString()
        }
      }
      params.push(val)
      const colName = this.pgsqlQuery.getColumnName(
        fieldName,
        null,
        null,
        false
      )
      setList.push(`${colName} = $${paramN++}`)
    })
    const sets = setList.join(", ")
    return { sets, params }
  }

  async delete(id, session) {
    const tableName = this.getTableName()
    const keyColumnName = this.getKeyColumnName()
    const objOut = await this.get(id)
    if (objOut) {
      const deleteStatement = `DELETE FROM ${tableName} WHERE ${keyColumnName} = $1`

      const queryResult = await this.db.pgClient.query(deleteStatement, [id])

      if (queryResult.rowCount == 1) {
        return objOut
      } else {
        throw new Error("Delete failed")
      }
    } else {
      throw new Error("Object does not exist")
    }
  }

  getKeyColumnName() {
    let keyColumnName = null

    const keyFieldName = this.type.getKeyField()
    if (keyFieldName) {
      const fields = this.type.typeSpec().fields
      const keyField = fields[keyFieldName]
      keyColumnName = keyField.asName ? keyField.asName : keyFieldName
    }
    return keyColumnName
  }

  genSelect(fieldNames, tableAlias) {
    const typeSpec = this.type.typeSpec()
    const fields = typeSpec.fields

    if (fieldNames.length == 0) {
      fieldNames = Object.keys(fields)
    }

    let selects = []
    fieldNames.forEach((fieldName) => {
      const fieldSpec = fields[fieldName]
      if (fieldSpec.type != ScalarTypes.ObjectRef && !fieldSpec.symbolic) {
        if (fieldSpec.asName) {
          selects.push(`${tableAlias}.${fieldSpec.asName} as "${fieldName}"`)
        } else {
          selects.push(`${tableAlias}.${fieldName}`)
        }
      }
    })
    const selectList = selects.join(", ")
    return selectList
  }

  /**
   * Perform M-M joins through intermediate tables.
   */
  async joinByIntermediate(joinName, joinKeyValue, limit = DEFAULT_LIMIT) {
    limit = limit || 1000

    const tableName = this.getTableName()
    const tableAlias = "t0"
    const tableKeyColumn = this.getKeyColumn()
    const defaultSort = this.getDefaultSort()
    if (!tableKeyColumn) {
      throw new Error(
        "manifest data.config.keyColumn must be set to the key column name"
      )
    }
    const joinSpec = this.getJoinSpec(joinName)
    if (!joinSpec) {
      throw Error(`join name not found: ${joinName}`)
    }
    const sqlSelect = this.genSelect([], tableAlias)
    const joinClause = ` JOIN ${joinSpec.table} tx on tx.${joinSpec.keyColumn} = t0.${tableKeyColumn} `

    const whereClause = ` WHERE tx.${joinSpec.joinColumn} = $1 `

    let orderByClause = ""
    if (defaultSort) {
      orderByClause = ` ORDER BY t0.${defaultSort} `
    }

    const query = `SELECT ${sqlSelect} FROM ${tableName} ${tableAlias} ${joinClause} ${whereClause} ${orderByClause} LIMIT ${limit}`
    // this.log.info(`Intermediate Join Query: ${query}`)
    const queryResult = await this.db.pgClient.query(query, [joinKeyValue])
    const rows = queryResult.rows

    let items = rows.map((row) => {
      return Object.create(this.type).loadData(row)
    })
    const result = {
      items: items,
      count: rows.length,
    }
    return result
  }
}
